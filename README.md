# Welcome to the new distribution site of ApicoAMP software! #

Please note that our [older site](https://code.google.com/p/apicoamp/) will no longer be maintained.

In order to access the previous and the latest versions of ApicoAMP software, please follow the [Downloads link](https://bitbucket.org/wsu_bcb/apicoamp/downloads) on the left.

In order to access the user manuals, please follow the [Wiki link](https://bitbucket.org/wsu_bcb/apicoamp/wiki/Home). Here are some direct links to the user manuals:

[ApicoAMP (version 1) User Manual](https://bitbucket.org/wsu_bcb/apicoamp/wiki/ApicoAMP%20(version%201)%20User%20Manual)

[Instructions for converting ApicoAMP_GUI.py to MAC OS X compatible .app file](https://bitbucket.org/wsu_bcb/apicoamp/wiki/Instructions%20for%20converting%20ApicoAMP_GUI.py%20to%20MAC%20OS%20X%20compatible%20.app%20file)


###Please report any bugs you encountered using the software to gokcen.cilingir@gmail.com###

**You can find more information over the method and the software at the following reference:**
[Cilingir, Gokcen, Audrey OT Lau, and Shira L. Broschat. "ApicoAMP: The first computational model for identifying apicoplast-targeted transmembrane proteins in Apicomplexa." Journal of microbiological methods 95.3 (2013): 313-319.](http://www.sciencedirect.com/science/article/pii/S016770121300300X)